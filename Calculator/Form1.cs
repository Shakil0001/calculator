﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        double value = 0;
        string operation = "";
        bool operation_pressed = false;
        string firstnum, secondnum;
        
        public Form1()
        {
            InitializeComponent();
        }
        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if ((result.Text == "0") || (operation_pressed))
                result.Text = "";
            operation_pressed = false;
            if (b.Text == ".")
            {
                if (!result.Text.Contains("."))
                    result.Text = result.Text + b.Text;
            }
            else
            {
                result.Text = result.Text + b.Text;
            }

        }
            
           

        private void button10_Click(object sender, EventArgs e)
        {
            result.Text = "0";
        }

        private void operator_click(object sender, EventArgs e)
        {
           
            Button b = (Button)sender;
            if (value != 0)
            {
                button20.PerformClick();
                operation_pressed = true;
                operation = b.Text;
                equation.Text = value + " " + operation;

            }
            else
            {
                operation = b.Text;
                value = double.Parse(result.Text);
                operation_pressed = true;
                equation.Text = value + " " + operation;
            }
            firstnum = equation.Text;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            secondnum = result.Text; 
            equation.Text = "";
            switch (operation)
            {
                case "+":
                    result.Text=(value + double.Parse(result.Text)).ToString();
                    break;
                case "-":
                    result.Text = (value - double.Parse(result.Text)).ToString();
                    break;
                case "*":
                    result.Text = (value * double.Parse(result.Text)).ToString();
                    break;
                case "/":
                    result.Text = (value / double.Parse(result.Text)).ToString();
                    break;
                default:
                    break;
                    
            }
            value = Double.Parse(result.Text);
            operation = " ";
            BtnClear.Visible = true;

            rbtdisplayhistory.AppendText( firstnum  + "   " + secondnum + "  =  " + "\n");

            rbtdisplayhistory.AppendText("\n\t" + result.Text + "\n\n");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            result.Text = "0";
            value = 0;
        }

        private void btnclear_click(object sender, EventArgs e)
        {
            rbtdisplayhistory.Clear();
            if (lblHistoryDisplay.Text == "")
            {
                lblHistoryDisplay.Text = "There's no history yet";
            }
            BtnClear.Visible = true;
            rbtdisplayhistory.ScrollBars = 0;
        }

        private void backspace_Click(object sender, EventArgs e)
        {
            if (result.Text.Length > 0)
            {

                result.Text = result.Text.Remove(result.Text.Length - 1, 1);
            }
            if (result.Text == "")
            {
                result.Text = "0";
            }
        }
    }
}
